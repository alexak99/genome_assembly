# creates seqFile for cactus-pangenome

import os

tsv_file_path = snakemake.output["tsv"]

fasta_directory = snakemake.config["references"]

fasta_files = [(os.path.splitext(filename)[0], os.path.join(fasta_directory, filename))
              for filename in os.listdir(fasta_directory)
              if filename.endswith(".fna") or filename.endswith(".fa") or filename.endswith(".fasta")]

with open(tsv_file_path, 'w') as tsv_file:
    for sample_name, file_path in fasta_files:
        tsv_file.write(f"{sample_name}\t{file_path}\n")