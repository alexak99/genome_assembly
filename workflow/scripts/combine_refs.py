import os
from Bio import SeqIO

input_folder = snakemake.config["references"]

output_file = snakemake.output["combined"]

combined_sequences = []

for filename in os.listdir(input_folder):
    if filename.endswith(".fna") or filename.endswith(".fa") or filename.endswith(".fasta"):
        filepath = os.path.join(input_folder, filename)

        for record in SeqIO.parse(filepath, "fasta"):
            combined_sequences.append(record)

SeqIO.write(combined_sequences, output_file, "fasta")
