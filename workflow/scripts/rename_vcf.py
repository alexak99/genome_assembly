# edits the sequence names in the vcf file so that it matches with the ID of the reference-sequence

vcf_filename = snakemake.input["vcf"]

with open(vcf_filename, 'r') as vcf_file:
    lines = vcf_file.readlines()

updated_lines = []

for line in lines:
    if line.startswith('#'):
        updated_lines.append(line)
    else:
        fields = line.strip().split('\t')
        fields[0] = fields[0].split('#')[-1]
        updated_lines.append('\t'.join(fields) + '\n')

new_vcf_filename = snakemake.output["out"]
with open(new_vcf_filename, 'w') as updated_file:
    updated_file.writelines(updated_lines)
