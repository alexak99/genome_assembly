import os
import shutil

source_folder = snakemake.input["dir"]

for filename in os.listdir(source_folder):
    if filename.startswith("ragtag.patch") and filename.endswith(".fasta"):
        source_path = os.path.join(source_folder, filename)
        destination_path = snakemake.output["patch"]

        shutil.copy(source_path, destination_path)
