# finds best reference sequence based on similarity to de-novo assemblies

import shutil

with open(snakemake.input["blast"], 'r') as blast_output:
    lines = blast_output.readlines()

bit_scores_dict = {}

for line in lines:
    columns = line.strip().split('\t')
    
    ref_id = columns[1]
    bit_score = float(columns[-1])
    
    if ref_id not in bit_scores_dict:
        bit_scores_dict[ref_id] = [bit_score]
    else:
        bit_scores_dict[ref_id].append(bit_score)

average_bit_scores = {ref_id: sum(scores) / len(scores) for ref_id, scores in bit_scores_dict.items()}

best_ref_id = max(average_bit_scores, key=average_bit_scores.get)

prefix = best_ref_id.split('.')[0]

path = f"{snakemake.config['references']}/{prefix}.fasta"

output = snakemake.output["best_ref"]

shutil.copy2(path, output)
