# edits reference-sequence-ID from fasta file obtained by vg_paths so that it matches with the ID of the original reference-sequence

with open(snakemake.input["ref"], "r") as input_file:
    fasta_lines = input_file.readlines()

new_id = f">{snakemake.config['pangenome_assembly']['build_pangenome_graph']['reference_name']}.1\n"

sequence = "".join(fasta_lines[1:])

with open(snakemake.output["ren_ref"], "w") as output_file:
    output_file.write(new_id)
    output_file.write(sequence)
