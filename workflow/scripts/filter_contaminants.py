import gzip
from Bio import SeqIO

threshold = snakemake.config["kraken"]["threshold"]
kraken_report = snakemake.input["report"]

# read the kraken report and identify taxa below the threshold
filtered_taxa = []
with open(kraken_report, "r") as report:
    for line in report:
        columns = line.split("\t")
        percent_coverage = float(columns[0])
        if percent_coverage >= threshold:
            filtered_taxa.append(columns[4])

fq1 = snakemake.input["fq1"]
fq2 = snakemake.input["fq2"]
kraken_classification = snakemake.input["classification"]

o1 = snakemake.output["o1"]
o2 = snakemake.output["o2"]

with gzip.open(o1, "wt") as output_1, gzip.open(o2, "wt") as output_2:
    # open fastq files
    with gzip.open(fq1, "rt") as fastq_1, gzip.open(fq2, "rt") as fastq_2, open(kraken_classification, "r") as classification:
        for record_1, record_2, kraken_line in zip(SeqIO.parse(fastq_1, "fastq"), SeqIO.parse(fastq_2, "fastq"), classification):
            # extract taxon-id from kraken2-output
            taxon_id = kraken_line.split('\t')[2]
            # test if taxon-id is in list of taxon-ids you want to keep
            if taxon_id in filtered_taxa:
                # write sequences to output files
                SeqIO.write(record_1, output_1, "fastq")
                SeqIO.write(record_2, output_2, "fastq")