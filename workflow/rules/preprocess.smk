def choose_fq1(wildcards):
    if config["preprocessing"]["run_host_filter"] and config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_1.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and config["preprocessing"]["run_kraken"]:
        return "results/preprocess/fastp_trimmed/{sample}_1.fastq.gz"

def choose_fq2(wildcards):
    if config["preprocessing"]["run_host_filter"] and config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_2.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and config["preprocessing"]["run_kraken"]:
        return "results/preprocess/fastp_trimmed/{sample}_2.fastq.gz"

def choose_fq_long(wildcards):
    if config["preprocessing"]["run_host_filter"] and config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_long.fastq"
    elif not config["preprocessing"]["run_host_filter"] and config["preprocessing"]["run_kraken"]:
        return samples.at[wildcards.sample,'long'] if wildcards.sample in samples.index else ''

rule fastp:
    input:
        r1 = lambda wildcards: samples.at[wildcards.sample,'fq1'] if 
        wildcards.sample in samples.index else '',
        r2 = lambda wildcards: samples.at[wildcards.sample,'fq2'] if 
        wildcards.sample in samples.index else ''
    output:
        o1 = "results/preprocess/fastp_trimmed/{sample}_1.fastq.gz",
        o2 = "results/preprocess/fastp_trimmed/{sample}_2.fastq.gz",
        html = "results/preprocess/fastp_trimmed/reports/{sample}_fastp.html",
        json = "results/preprocess/fastp_trimmed/reports/{sample}_fastp.json"
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/fastp/{sample}.log"
    benchmark: "results/benchmarks/preprocess/fastp/{sample}.txt"
    threads: config["threads"]
    shell:
        "fastp -i {input.r1} -I {input.r2} -o {output.o1} -O {output.o2} -h {output.html} -j {output.json} -w {threads} 2> {log}"

rule multiqc:
    input:
        expand("results/preprocess/fastp_trimmed/reports/{sample}_fastp.html", sample = list(samples.index)),
        expand("results/preprocess/fastp_trimmed/reports/{sample}_fastp.json", sample = list(samples.index))
    output:
        "results/preprocess/multiqc/multiqc_report.html"
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/multiqc/multiqc.log"
    benchmark: "results/benchmarks/preprocess/multiqc/multiqc.txt"
    shell:
        "multiqc results/preprocess/fastp_trimmed/reports -o results/preprocess/multiqc -f 2> {log}"

# short reads

rule build_index:
    input:
        config["preprocessing"]["host"]["sequence"]
    output:
        multiext(config["preprocessing"]["host"]["index"], ".1.bt2", ".2.bt2",".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2")
    params:
        config["preprocessing"]["host"]["index"]
    conda:
        "../envs/preprocess.yaml"
    log: "results/logs/preprocess/build_index/build_index.log"
    benchmark: "results/benchmarks/preprocess/build_index/build_index.txt"
    threads: config["threads"]
    shell:
        "bowtie2-build --threads {threads} {input} {params} 2> {log}"

rule bowtie:
    input:
        fq1 = rules.fastp.output[0],
        fq2 = rules.fastp.output[1],
        rf = rules.build_index.output
    output:
        sam = "results/preprocess/sam_host/{sample}.sam"
    params:
        n = config["preprocessing"]["bowtie2"]["n_max_mismatches"],
        l = config["preprocessing"]["bowtie2"]["len_seed_substr"],
        index=config["preprocessing"]["host"]["index"]
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/bowtie/{sample}.log"
    benchmark: "results/benchmarks/preprocess/bowtie/{sample}.txt"
    threads: config["threads"]
    shell:
        "bowtie2 -N {params.n} -L {params.l} --threads {threads} -x {params.index} -1 {input.fq1} -2 {input.fq2} -S {output} 2> {log}"

rule to_fastq:
    input:
        rules.bowtie.output
    output:
        fa1 = "results/preprocess/mapping_filtered/{sample}_1.fastq.gz",
        fa2 = "results/preprocess/mapping_filtered/{sample}_2.fastq.gz"
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/to_fastq/{sample}.log"
    benchmark: "results/benchmarks/preprocess/to_fastq/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools fastq -1 {output.fa1} -2 {output.fa2} -f 12 -@ {threads} {input} 2> {log}"

rule kraken:
    input:
        choose_fq1,
        choose_fq2
    output:
        classification = "results/preprocess/kraken/classification/{sample}.tsv",
        report = "results/preprocess/kraken/reports/{sample}.tsv"
    params:
        config["preprocessing"]["kraken"]["kraken_database"]
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/kraken/{sample}.log"
    benchmark: "results/benchmarks/preprocess/kraken/{sample}.txt"
    threads: config["threads"]
    shell:
        "kraken2 --db {params} --threads {threads} --output {output.classification} --report {output.report} --gzip-compressed -- paired {input} 2> {log}"

rule filter_contaminants:
    input:
        fa1 = rules.to_fastq.output[0],
        fa2 = rules.to_fastq.output[1],
        classification = rules.kraken.output[0]
    output:
        fa1 = "results/preprocess/kraken_filtered/{sample}_1.fastq.gz",
        fa2 = "results/preprocess/kraken_filtered/{sample}_2.fastq.gz"
    params:
        tax = config["preprocessing"]["kraken"]["keep_taxa"],
        fa1 = "results/preprocess/kraken_filtered/{sample}_1.fastq",
        fa2 = "results/preprocess/kraken_filtered/{sample}_2.fastq"
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/filter_contaminants/{sample}.log"
    benchmark: "results/benchmarks/preprocess/filter_contaminants/{sample}.txt"
    shell:
        "python $CONDA_PREFIX/bin/extract_kraken_reads.py -k {input.classification} -1 {input.fa1} -2 {input.fa2} -o {params.fa1} -o2 {params.fa2} --fastq-output -t {params.tax} && gzip {params.fa1} {params.fa2} 2> {log}"

# long reads

rule minimap:
    input:
        ref = config["preprocessing"]["host"]["sequence"],
        fq = lambda wildcards: samples.at[wildcards.sample,'long'] if 
        wildcards.sample in samples.index else ''
    output: "results/preprocess/minimap/{sample}.sam"
    params: config["preprocessing"]["minimap"]["options"]
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/minimap/{sample}.log"
    benchmark: "results/benchmarks/preprocess/minimap/{sample}.txt"
    threads: config["threads"]
    shell:
        "minimap2 {params} -t {threads} -a {input.ref} {input.fq} > {output} 2> {log}"

rule to_fastq_long:
    input:
        rules.minimap.output
    output: "results/preprocess/mapping_filtered/{sample}_long.fastq"
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/to_fastq_long/{sample}.log"
    benchmark: "results/benchmarks/preprocess/to_fastq_long/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools fastq -f 4 -@ {threads} {input} > {output} 2> {log}"

rule kraken_long:
    input:
        choose_fq_long
    output:
        classification = "results/preprocess/kraken_long/classification/{sample}.tsv",
        report = "results/preprocess/kraken_long/reports/{sample}.tsv"
    params:
        config["preprocessing"]["kraken"]["kraken_database"]
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/kraken_long/{sample}.log"
    benchmark: "results/benchmarks/preprocess/kraken_long/{sample}.txt"
    threads: config["threads"]
    shell:
        "kraken2 --db {params} --threads {threads} --output {output.classification} --report {output.report} {input} 2> {log}"

rule filter_contaminants_long:
    input:
        fq = rules.to_fastq_long.output,
        classification = rules.kraken_long.output[0]
    output: "results/preprocess/kraken_filtered/{sample}_long.fastq.gz"
    params:
        tax = config["preprocessing"]["kraken"]["keep_taxa"],
        fq = "results/preprocess/kraken_filtered/{sample}_long.fastq"
    conda: "../envs/preprocess.yaml"
    log: "results/logs/preprocess/filter_contaminants_long/{sample}.log"
    benchmark: "results/benchmarks/preprocess/filter_contaminants_long/{sample}.txt"
    shell:
        "python $CONDA_PREFIX/bin/extract_kraken_reads.py -k {input.classification} -s {input.fq} -o {params.fq} --fastq-output -t {params.tax} && gzip {params.fq} 2> {log}"