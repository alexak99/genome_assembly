"""
rule setup_cactus:
    output: touch("results/cactus/setup_complete.txt")
    shell:
        \"""
        cd results/cactus
        wget https://github.com/ComparativeGenomicsToolkit/cactus/releases/download/v2.6.8/cactus-bin-v2.6.8.tar.gz
        tar -xzf cactus-bin-v2.6.8.tar.gz
        cd cactus-bin-v2.6.8
        virtualenv -p python3 venv-cactus-v2.6.8
        printf "export PATH=$(pwd)/bin:\$PATH\nexport PYTHONPATH=$(pwd)/lib:\$PYTHONPATH\n" >> venv-cactus-v2.6.8/bin/activate
        source venv-cactus-v2.6.8/bin/activate
        python3 -m pip install -U setuptools pip
        python3 -m pip install -U .
        python3 -m pip install -U -r ./toil-requirement.txt
        \"""
"""

def choose_contigs(wildcards):
    if config["long_reads"]["additional"]:
        return "results/denovo_assembly/SPAdes_long/{sample}/contigs.fasta"
    elif config["long_reads"]["only_long_reads"]:
        return "results/denovo_assembly/flye/{sample}/assembly.fasta"
    else:
        return "results/denovo_assembly/{sample}/ren_combined.fasta"

def choose_fa1(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_1.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_1.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_1.fastq.gz"

def choose_fa2(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_2.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_2.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_2.fastq.gz"

def choose_long(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_long.fastq"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return samples.at[wildcards.sample,'long'] if wildcards.sample in samples.index else ''
    else:
        return "results/preprocess/kraken_filtered/{sample}_long.fastq.gz"

rule minigraph_tsv:
    output: 
        tsv="results/pangenome_graph/samples.tsv"
    benchmark: "results/benchmarks/pangenome_graph/minigraph_tsv/tsv.txt"
    script:
        "../scripts/minigraph_tsv.py"

rule cactus_pangenome:
    input:
        samples=rules.minigraph_tsv.output
#        cactus=rules.setup_cactus.output
    output: config["pangenome_assembly"]["pangenome_graph"]
    params: 
        cactus=config["pangenome_assembly"]["build_pangenome_graph"]["cactus_env"],
        ref=config["pangenome_assembly"]["build_pangenome_graph"]["reference_name"]
    log: "results/logs/pangenome_graph/cactus_pangenome/pangenome.log"
    benchmark: "results/benchmarks/pangenome_graph/cactus_pangenome/pangenome.txt"
    threads: config["threads"]
    shell:
        "source {params.cactus}/bin/activate && cactus-pangenome results/pangenome_graph/cactus_pangenome_metadata {input.samples} --outDir results/pangenome_graph/cactus_pangenome --outName pangenome --reference {params.ref} --defaultCores {threads} --maxCores {threads} 2> {log}"

rule gunzip:
    input:
        rules.cactus_pangenome.output
    output: "results/pangenome_graph/cactus_pangenome/pangenome.gfa"
    log: "results/logs/pangenome_graph/gunzip/gunzip.log"
    benchmark: "results/benchmarks/pangenome_graph/gunzip/gunzip.txt"
    shell:
        "gunzip {input} 2> {log}"

rule vg_autoindex:
    input:
        rules.gunzip.output
    output: "results/pangenome_graph/cactus_pangenome/index.giraffe.gbz"
    params: 
        dir="results/pangenome_graph/cactus_pangenome",
        gfa=os.path.join(os.getcwd(), "results/pangenome_graph/cactus_pangenome/pangenome.gfa")
    conda: "../envs/pangenome_graph.yaml"
    log: os.path.join(os.getcwd(), "results/logs/pangenome_graph/vg_autoindex/autoindex.log")
    benchmark: "results/benchmarks/pangenome_graph/vg_autoindex/autoindex.txt"
    threads: config["threads"]
    shell:
        """
        cd {params.dir}
        vg autoindex --workflow giraffe -g {params.gfa} -t {threads} 2> {log}
        """

rule vg_giraffe:
    input:
        gbz=rules.vg_autoindex.output,
        fa1 = choose_fa1,
        fa2 = choose_fa2
    output: "results/pangenome_graph/{sample}/mapped.bam"
    params:
        sample="{sample}",
        opt=config["pangenome_assembly"]["vg_giraffe_options"]["reads"]
    conda: "../envs/pangenome_graph.yaml"
    log: "results/logs/pangenome_graph/vg_giraffe/{sample}.log"
    benchmark: "results/benchmarks/pangenome_graph/vg_giraffe/{sample}.txt"
    threads: config["threads"]
    shell:
        "vg giraffe -Z {input.gbz} -t {threads} -f {input.fa1} -f {input.fa2} -N {params.sample} -R {params.sample} -o BAM {params.opt} | samtools sort -o {output} -O bam -@ {threads} 2> {log}"

rule vg_giraffe_long:
    input:
        gbz=rules.vg_autoindex.output,
        long=choose_long
    output: "results/pangenome_graph/{sample}/mapped_long.bam"
    params:
        sample="{sample}",
        opt=config["pangenome_assembly"]["vg_giraffe_options"]["long_reads"]
    conda: "../envs/pangenome_graph.yaml"
    log: "results/logs/pangenome_graph/vg_giraffe_long/{sample}.log"
    benchmark: "results/benchmarks/pangenome_graph/vg_giraffe_long/{sample}.txt"
    threads: config["threads"]
    shell:
        "vg giraffe -Z {input.gbz} -t {threads} -f {input.long} -N {params.sample} -R {params.sample} -o BAM {params.opt} | samtools sort -o {output} -O bam -@ {threads} 2> {log}"

rule vg_giraffe_contigs:
    input:
        gbz=rules.vg_autoindex.output,
        contigs=choose_contigs
    output: "results/pangenome_graph/{sample}/mapped_contigs.bam"
    params:
        sample="{sample}",
        opt=config["pangenome_assembly"]["vg_giraffe_options"]["contigs"]
    conda: "../envs/pangenome_graph.yaml"
    log: "results/logs/pangenome_graph/vg_giraffe_contigs/{sample}.log"
    benchmark: "results/benchmarks/pangenome_graph/vg_giraffe_contigs/{sample}.txt"
    threads: config["threads"]
    shell:
        "vg giraffe -Z {input.gbz} -t {threads} -f {input.contigs} -N {params.sample} -R {params.sample} -o BAM {params.opt} | samtools sort -o {output} -O bam -@ {threads} 2> {log}"
