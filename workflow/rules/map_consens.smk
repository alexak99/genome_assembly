def choose_fa1(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_1.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_1.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_1.fastq.gz"

def choose_fa2(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_2.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_2.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_2.fastq.gz"

def choose_long(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_long.fastq"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return samples.at[wildcards.sample,'long'] if wildcards.sample in samples.index else ''
    else:
        return "results/preprocess/kraken_filtered/{sample}_long.fastq.gz"

def choose_sam_consens(wildcards):
    if config["long_reads"]["additional"]:
        return "results/map_consens/{sample}/merged.sam"
    elif config["long_reads"]["only_long_reads"]:
        return "results/map_consens/{sample}/minimap.sam"
    else:
        return "results/map_consens/bwa/{sample}.sam"

rule bwa_index_consens:
    input: "results/variant_calling/{sample}/freebayes/consensus_freebayes.fa"
    output: 
        multiext("results/map_consens/{sample}/bwa_index/consensus_freebayes", ".amb", ".ann",".bwt", ".pac", ".sa")
    params: "results/map_consens/{sample}/bwa_index/consensus_freebayes"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/bwa_index_consens/{sample}.log"
    benchmark: "results/benchmarks/map_consens/bwa_index_consens/{sample}.txt"
    shell:
        "bwa index -p {params} {input} 2> {log}"

rule bwa_consens:
    input:
        fa1 = choose_fa1,
        fa2 = choose_fa2,
        idx= rules.bwa_index_consens.output
    output: "results/map_consens/bwa/{sample}.sam"
    params: 
        sample="{sample}",
        ref="results/map_consens/{sample}/bwa_index/consensus_freebayes"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/bwa_consens/{sample}.log"
    benchmark: "results/benchmarks/map_consens/bwa_consens/{sample}.txt"
    threads: config["threads"]
    shell:
        "bwa mem -t {threads} -o {output} -R '@RG\\tID:{params.sample}\\tSM:{params.sample}' {params.ref} {input.fa1} {input.fa2} 2> {log}"

rule sort_sam_consens:
    input:
        choose_sam_consens
    output: "results/map_consens/{sample}/sorted.bam"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/sort_sam/{sample}.log"
    benchmark: "results/benchmarks/map_consens/sort_sam/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools sort -o {output} -O bam -@ {threads} {input} 2> {log}"

rule index_bam_consens:
    input:
        rules.sort_sam_consens.output
    output: "results/map_consens/{sample}/sorted.bam.bai"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/index_bam_consens/{sample}.log"
    benchmark: "results/benchmarks/map_consens/index_bam_consens/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools index -@ {threads} {input} 2> {log}"

# long reads

rule minimap_consens:
    input:
        ref = "results/variant_calling/{sample}/freebayes/consensus_freebayes.fa",
        fq = choose_long
    output: "results/map_consens/{sample}/minimap.sam"
    params:
        sample="{sample}",
        opt=config["pangenome_assembly"]["minimap"]["options"]
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/minimap/{sample}.log"
    benchmark: "results/benchmarks/map_consens/minimap/{sample}.txt"
    threads: config["threads"]
    shell:
        "minimap2 {params.opt} -R '@RG\\tID:{params.sample}\\tSM:{params.sample}' -t {threads} -a {input.ref} {input.fq} > {output} 2> {log}"

rule merge_sam_consens:
    input:
        bwa=rules.bwa_consens.output,
        minim=rules.minimap_consens.output
    output: "results/map_consens/{sample}/merged.sam"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/merge_sam/{sample}.log"
    benchmark: "results/benchmarks/map_consens/merge_sam/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools merge -o {output} -@ {threads} {input} 2> {log}"

rule faidx_consens:
    input: "results/variant_calling/{sample}/freebayes/consensus_freebayes.fa"
    output: "results/map_consens/{sample}/best_ref.fna.fai"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/faidx_ref_based/{sample}.log"
    benchmark: "results/benchmarks/map_consens/faidx_ref_based/{sample}.txt"
    shell:
        "samtools faidx {input} -o {output} 2> {log}"

# variant calling

rule freebayes_consens:
    input:
        bam=rules.sort_sam_consens.output,
        bai=rules.index_bam_consens.output,
        ref="results/variant_calling/{sample}/freebayes/consensus_freebayes.fa"
    output: "results/map_consens/{sample}/freebayes/var.vcf"
    params: 
        opt=config["pangenome_assembly"]["variant_calling"]["freebayes_options"]
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/freebayes/freebayes/{sample}.log"
    benchmark: "results/benchmarks/map_consens/freebayes/freebayes/{sample}.txt"
    shell:
        "freebayes -f {input.ref} -v {output} -d {params.opt} {input.bam} 2> {log}"

rule rename_vcf_freebayes_consens:
    input:
        vcf="results/map_consens/{sample}/freebayes/var.vcf"
    output: 
        out="results/map_consens/{sample}/freebayes/ren.vcf"
    conda: "../envs/map_consens.yaml"
    benchmark: "results/benchmarks/map_consens/freebayes/rename_vcf_freebayes/{sample}.txt"
    script:
        "../scripts/rename_vcf.py"

rule filter_variants_freebayes_consens:
    input:
        rules.rename_vcf_freebayes_consens.output
    output: "results/map_consens/{sample}/freebayes/filtered_var.vcf.gz"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/freebayes/filter_variants_freebayes/{sample}.log"
    benchmark: "results/benchmarks/map_consens/freebayes/filter_variants_freebayes/{sample}.txt"
    threads: config["threads"]
    shell:
        "bcftools view -o {output} -i 'QUAL>=30 & INFO/DP>=20' -O z --threads {threads} {input} 2> {log}"

rule index_vcf_freebayes_consens:
    input:
        rules.filter_variants_freebayes_consens.output
    output: "results/map_consens/{sample}/freebayes/filtered_var.vcf.gz.tbi"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/freebayes/index_vcf_freebayes/{sample}.log"
    benchmark: "results/benchmarks/map_consens/freebayes/index_vcf_freebayes/{sample}.txt"
    shell:
        "tabix -p vcf {input} 2> {log}"
    
rule consensus_freebayes_consens:
    input:
        vcf=rules.filter_variants_freebayes_consens.output,
        tbi = rules.index_vcf_freebayes_consens.output,
        ref="results/variant_calling/{sample}/freebayes/consensus_freebayes.fa"
    output: "results/map_consens/{sample}/freebayes/consensus_freebayes.fa"
    conda: "../envs/map_consens.yaml"
    log: "results/logs/map_consens/freebayes/consensus_freebayes/{sample}.log"
    benchmark: "results/benchmarks/map_consens/freebayes/consensus_freebayes/{sample}.txt"
    shell:
        "cat {input.ref} | bcftools consensus {input.vcf} > {output} 2> {log}"