def choose_bam(wildcards):
    if config["long_reads"]["additional"]:
        if config["run_denovo_assembly"]:
            return "results/variant_calling/{sample}/merged_long_contigs.bam"
        else:
            return "results/variant_calling/{sample}/merged_long.bam"
    else:
        if config["run_denovo_assembly"]:
            return "results/variant_calling/{sample}/merged.bam"
        else:
            if config["long_reads"]["only_long_reads"]:
                return "results/pangenome_graph/{sample}/mapped_long.bam"
            else:
                return "results/pangenome_graph/{sample}/mapped.bam"

def choose_reads(wildcards):
    if config["long_reads"]["only_long_reads"]:
        return "results/pangenome_graph/{sample}/mapped_long.bam"
    else:
        return "results/pangenome_graph/{sample}/mapped.bam"

rule vg_paths:
    input: "results/pangenome_graph/cactus_pangenome/index.giraffe.gbz"
    output: "results/variant_calling/ref_path.fa"
    params: config["pangenome_assembly"]["build_pangenome_graph"]["reference_name"]
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/vg_paths/paths.log"
    benchmark: "results/benchmarks/variant_calling/vg_paths/paths.txt"
    shell:
        "vg paths --extract-fasta -x {input} --paths-by {params} > {output} 2> {log}"

rule rename_seq:
    input:
        ref="results/variant_calling/ref_path.fa"
    output:
        ren_ref="results/pangenome_graph/ren_ref.fa"
    benchmark: "results/benchmarks/variant_calling/rename_seq/ren_seq.txt"
    script:
        "../scripts/rename_seq.py"

rule merge_bam:
    input:
        reads=choose_reads,
        contigs="results/pangenome_graph/{sample}/mapped_contigs.bam"
    output: "results/variant_calling/{sample}/merged.bam"
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/surject_to_bam/merge_bam/{sample}.log"
    benchmark: "results/benchmarks/variant_calling/surject_to_bam/merge_bam/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools merge -o {output} -@ {threads} {input} 2> {log}"

rule merge_bam_long:
    input:
        short="results/pangenome_graph/{sample}/mapped.bam",
        long="results/pangenome_graph/{sample}/mapped_long.bam"
    output: "results/variant_calling/{sample}/merged_long.bam"
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/surject_to_bam/merge_bam_long/{sample}.log"
    benchmark: "results/benchmarks/variant_calling/surject_to_bam/merge_bam_long/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools merge -o {output} -@ {threads} {input} 2> {log}"

rule merge_bam_long_contigs:
    input:
        short="results/pangenome_graph/{sample}/mapped.bam",
        contigs="results/pangenome_graph/{sample}/mapped_contigs.bam",
        long="results/pangenome_graph/{sample}/mapped_long.bam"
    output: "results/variant_calling/{sample}/merged_long_contigs.bam"
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/surject_to_bam/merge_bam_long/{sample}.log"
    benchmark: "results/benchmarks/variant_calling/surject_to_bam/merge_bam_long/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools merge -o {output} -@ {threads} {input} 2> {log}"

# FreeBayes

rule freebayes:
    input:
        bam=choose_bam,
        ref=rules.vg_paths.output
    output: "results/variant_calling/{sample}/freebayes/var.vcf"
    params: 
        opt=config["pangenome_assembly"]["variant_calling"]["freebayes_options"]
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/freebayes/freebayes/{sample}.log"
    benchmark: "results/benchmarks/variant_calling/freebayes/freebayes/{sample}.txt"
    shell:
        "freebayes -f {input.ref} -v {output} -d {params.opt} {input.bam} 2> {log}"

rule rename_vcf_freebayes:
    input:
        vcf="results/variant_calling/{sample}/freebayes/var.vcf"
    output: 
        out="results/variant_calling/{sample}/freebayes/ren.vcf"
    conda: "../envs/variant_calling.yaml"
    benchmark: "results/benchmarks/variant_calling/freebayes/rename_vcf_freebayes/{sample}.txt"
    script:
        "../scripts/rename_vcf.py"

rule filter_variants_freebayes:
    input:
        vcf=rules.rename_vcf_freebayes.output
    output: "results/variant_calling/{sample}/freebayes/filtered_var.vcf.gz"
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/freebayes/filter_variants_freebayes/{sample}.log"
    benchmark: "results/benchmarks/variant_calling/freebayes/filter_variants_freebayes/{sample}.txt"
    threads: config["threads"]
    shell:
        "bcftools view -o {output} -i 'QUAL>=30 & INFO/DP>=20' -O z --threads {threads} {input.vcf} 2> {log}"

rule index_vcf_freebayes:
    input:
        rules.filter_variants_freebayes.output
    output: "results/variant_calling/{sample}/freebayes/filtered_var.vcf.gz.tbi"
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/freebayes/index_vcf_freebayes/{sample}.log"
    benchmark: "results/benchmarks/variant_calling/freebayes/index_vcf_freebayes/{sample}.txt"
    shell:
        "tabix -p vcf {input} 2> {log}"
    
rule consensus_freebayes:
    input:
        vcf=rules.filter_variants_freebayes.output,
        tbi = rules.index_vcf_freebayes.output,
        ref=rules.rename_seq.output
    output: "results/variant_calling/{sample}/freebayes/consensus_freebayes.fa"
    conda: "../envs/variant_calling.yaml"
    log: "results/logs/variant_calling/freebayes/consensus_freebayes/{sample}.log"
    benchmark: "results/benchmarks/variant_calling/freebayes/consensus_freebayes/{sample}.txt"
    shell:
        "cat {input.ref} | bcftools consensus {input.vcf} > {output} 2> {log}"