def choose_assembly(wildcards):
    if config["long_reads"]["additional"]:
        return "results/denovo_assembly/SPAdes_long/{sample}/contigs.fasta"
    elif config["long_reads"]["only_long_reads"]:
        return "results/denovo_assembly/flye/{sample}/assembly.fasta"
    else:
        return "results/denovo_assembly/{sample}/ren_combined.fasta"

def choose_sam(wildcards):
    if config["long_reads"]["additional"]:
        return "results/ref_based_assembly/{sample}/merged.sam"
    elif config["long_reads"]["only_long_reads"]:
        return "results/ref_based_assembly/{sample}/minimap.sam"
    else:
        return "results/ref_based_assembly/bwa/{sample}.sam"

def choose_ref(wildcards):
    if config["ref_based_assembly"]["patch"]:
        return "results/ref_based_assembly/{sample}/ragtag_output/ragtag.patch.fasta"
    else:
        return "results/ref_based_assembly/{sample}/best_ref.fasta"

def choose_fa1(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_1.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_1.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_1.fastq.gz"

def choose_fa2(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_2.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_2.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_2.fastq.gz"

def choose_long(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_long.fastq"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return samples.at[wildcards.sample,'long'] if wildcards.sample in samples.index else ''
    else:
        return "results/preprocess/kraken_filtered/{sample}_long.fastq.gz"

rule combine_refs:
    output:
        combined="results/ref_based_assembly/combined_refs.fasta"
    conda: "../envs/ref_based_assembly.yaml"
    benchmark: "results/benchmarks/ref_based_assembly/combine_refs/combine_refs.txt"
    script:
        "../scripts/combine_refs.py"

rule make_blast_db:
    input:
        rules.combine_refs.output
    output:
        multiext("results/ref_based_assembly/blast_db/db", ".nhr", ".nin",".nsq")
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/make_blast_db/make_blast_db.log"
    benchmark: "results/benchmarks/ref_based_assembly/make_blast_db/make_blast_db.txt"
    shell:
        "makeblastdb -in {input} -dbtype nucl -out results/ref_based_assembly/blast_db/db 2> {log}"

rule blast:
    input:
        db=rules.make_blast_db.output,
        query=choose_assembly
    output: "results/ref_based_assembly/{sample}/blast.out"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/blast/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/blast/{sample}.txt"
    threads: config["threads"]
    shell:
        "blastn -db results/ref_based_assembly/blast_db/db -query {input.query} -out {output} -outfmt 6 -num_threads {threads} 2> {log}"

rule best_ref:
    input:
        blast="results/ref_based_assembly/{sample}/blast.out"
    output:
        best_ref="results/ref_based_assembly/{sample}/best_ref.fasta"
    benchmark: "results/benchmarks/ref_based_assembly/best_ref/{sample}.txt"
    script:
        "../scripts/best_ref.py"

rule patch:
    input:
        ref=rules.best_ref.output,
        contigs=choose_assembly
    output: "results/ref_based_assembly/{sample}/ragtag_output/ragtag.patch.fasta"
    params: "results/ref_based_assembly/{sample}/ragtag_output"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/patch/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/patch/{sample}.txt"
    threads: config["threads"]
    shell:
        "ragtag.py patch {input.contigs} {input.ref} -o {params} -t {threads} 2> {log}"

# short reads

rule bwa_index:
    input:
        choose_ref
    output:
        multiext("results/ref_based_assembly/{sample}/bwa_index/best_ref", ".amb", ".ann",".bwt", ".pac", ".sa")
    params: "results/ref_based_assembly/{sample}/bwa_index/best_ref"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/bwa_index/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/bwa_index/{sample}.txt"
    shell:
        "bwa index -p {params} {input} 2> {log}"

rule bwa:
    input:
        fa1 = choose_fa1,
        fa2 = choose_fa2,
        idx= rules.bwa_index.output
    output: "results/ref_based_assembly/bwa/{sample}.sam"
    params: 
        sample="{sample}",
        ref="results/ref_based_assembly/{sample}/bwa_index/best_ref"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/bwa/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/bwa/{sample}.txt"
    threads: config["threads"]
    shell:
        "bwa mem -t {threads} -o {output} -R '@RG\\tID:{params.sample}\\tSM:{params.sample}' {params.ref} {input.fa1} {input.fa2} 2> {log}"

# long reads

rule minimap_ref:
    input:
        ref = choose_ref,
        fq = choose_long
    output: "results/ref_based_assembly/{sample}/minimap.sam"
    params:
        sample="{sample}",
        opt=config["ref_based_assembly"]["minimap"]["options"]
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/minimap/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/minimap/{sample}.txt"
    threads: config["threads"]
    shell:
        "minimap2 {params.opt} -R '@RG\\tID:{params.sample}\\tSM:{params.sample}' -t {threads} -a {input.ref} {input.fq} > {output} 2> {log}"

rule merge_sam:
    input:
        bwa=rules.bwa.output,
        minim=rules.minimap_ref.output
    output: "results/ref_based_assembly/{sample}/merged.sam"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/merge_sam/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/merge_sam/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools merge -o {output} -@ {threads} {input} 2> {log}"

rule sort_sam:
    input:
        choose_sam
    output: "results/ref_based_assembly/{sample}/sorted.bam"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/sort_sam/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/sort_sam/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools sort -o {output} -O bam -@ {threads} {input} 2> {log}"

rule index_bam_ref_based:
    input:
        rules.sort_sam.output
    output: "results/ref_based_assembly/{sample}/sorted.bam.bai"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/index_bam_ref_based/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/index_bam_ref_based/{sample}.txt"
    threads: config["threads"]
    shell:
        "samtools index -@ {threads} {input} 2> {log}"

rule faidx_ref_based:
    input:
        choose_ref
    output: "results/ref_based_assembly/{sample}/ragtag.patch.fasta.fai"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/faidx_ref_based/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/faidx_ref_based/{sample}.txt"
    shell:
        "samtools faidx {input} -o {output} 2> {log}"

# freebayes

rule freebayes_ref_based:
    input:
        ref=choose_ref,
        bam=rules.sort_sam.output,
        bai=rules.index_bam_ref_based.output,
        fai=rules.faidx_ref_based.output
    output: "results/ref_based_assembly/{sample}/freebayes/var.vcf"
    params: config["ref_based_assembly"]["freebayes_options"]
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/freebayes/freebayes_ref_based/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/freebayes/freebayes_ref_based/{sample}.txt"
    shell:
        "freebayes -f {input.ref} -v {output} -d {params} {input.bam} 2> {log}"

rule filter_variants_freebayes_ref_based:
    input:
        vcf=rules.freebayes_ref_based.output
    output: "results/ref_based_assembly/{sample}/freebayes/filtered_var.vcf.gz"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/freebayes/filter_variants_freebayes_ref_based/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/freebayes/filter_variants_freebayes_ref_based/{sample}.txt"
    threads: config["threads"]
    shell:
        "bcftools view -o {output} -i 'QUAL>=30 & INFO/DP>=20' -O z --threads {threads} {input.vcf} 2> {log}"

rule index_vcf_freebayes_ref_based:
    input:
        rules.filter_variants_freebayes_ref_based.output
    output: "results/ref_based_assembly/{sample}/freebayes/filtered_var.vcf.gz.tbi"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/freebayes/index_vcf_freebayes_ref_based/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/freebayes/index_vcf_freebayes_ref_based/{sample}.txt"
    shell:
        "tabix -p vcf {input} 2> {log}"

rule depth:
    input:
        rules.sort_sam.output
    output: "results/ref_based_assembly/{sample}/low_coverage.bed"
    params: config["ref_based_assembly"]["coverage_threshold"]
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/depth/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/depth/{sample}.txt"
    shell:
        "samtools depth -a {input} | awk '$3 < {params} {{print $1\"\t\"$2\"\t\"$2+1}}' > {output}"

    
rule consensus_freebayes_ref_based:
    input:
        ref=choose_ref,
        vcf=rules.filter_variants_freebayes_ref_based.output,
        tbi = rules.index_vcf_freebayes_ref_based.output,
        bed= rules.depth.output
    output: "results/ref_based_assembly/{sample}/consensus_freebayes.fa"
    conda: "../envs/ref_based_assembly.yaml"
    log: "results/logs/ref_based_assembly/freebayes/consensus_freebayes_ref_based/{sample}.log"
    benchmark: "results/benchmarks/ref_based_assembly/freebayes/consensus_freebayes_ref_based/{sample}.txt"
    shell:
        "cat {input.ref} | bcftools consensus --mask {input.bed} {input.vcf} > {output} 2> {log}"