import os

def choose_fa1(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_1.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_1.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_1.fastq.gz"

def choose_fa2(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_2.fastq.gz"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return "results/preprocess/fastp_trimmed/{sample}_2.fastq.gz"
    else:
        return "results/preprocess/kraken_filtered/{sample}_2.fastq.gz"

def choose_fa1_abyss(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return os.path.join(os.getcwd(),b"results/preprocess/mapping_filtered/{sample}_1.fastq.gz")
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return os.path.join(os.getcwd(), "results/preprocess/fastp_trimmed/{sample}_1.fastq.gz")
    else:
        return os.path.join(os.getcwd(), "results/preprocess/kraken_filtered/{sample}_1.fastq.gz")

def choose_fa2_abyss(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return os.path.join(os.getcwd(), "results/preprocess/mapping_filtered/{sample}_2.fastq.gz")
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return os.path.join(os.getcwd(), "results/preprocess/fastp_trimmed/{sample}_2.fastq.gz")
    else:
        return os.path.join(os.getcwd(), "results/preprocess/kraken_filtered/{sample}_2.fastq.gz")

def choose_long(wildcards):
    if config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]:
        return "results/preprocess/mapping_filtered/{sample}_long.fastq"
    elif not config["preprocessing"]["run_host_filter"] and not config["preprocessing"]["run_kraken"]: 
        return samples.at[wildcards.sample, 'long'] if wildcards.sample in samples.index else ''
    else:
        return "results/preprocess/kraken_filtered/{sample}_long.fastq.gz"

# only short reads

rule spades:
    input:
        fa1 = choose_fa1,
        fa2 = choose_fa2
    output:
        contigs = "results/denovo_assembly/SPAdes/{sample}/contigs.fasta"
    params:
        out_dir = "results/denovo_assembly/SPAdes/{sample}",
        options = config["denovo_assembly"]["SPAdes"]["options"]
    conda: "../envs/denovo_assembly.yaml"
    log: "results/logs/denovo_assembly/SPAdes/{sample}.log"
    benchmark: "results/benchmarks/denovo_assembly/spades/{sample}.txt"
    threads: config["threads"]
    shell:
        "spades.py -1 {input.fa1} -2 {input.fa2} -t {threads} {params.options} -o {params.out_dir} 2> {log}"

rule velveth:
    input:
        fa1 = choose_fa1,
        fa2 = choose_fa2
    output:
        roadmaps = "results/denovo_assembly/velvet/{sample}/Roadmaps",
        sequences = "results/denovo_assembly/velvet/{sample}/Sequences"
    params:
        out_dir = "results/denovo_assembly/velvet/{sample}",
        k_mer_length = config["denovo_assembly"]["Velvet"]["k_mer"],
        options = config["denovo_assembly"]["Velvet"]["velveth_options"]
    conda: "../envs/denovo_assembly.yaml"
    log: "results/logs/denovo_assembly/velveth/{sample}.log"
    benchmark: "results/benchmarks/denovo_assembly/velveth/{sample}.txt"
    shell:
        "velveth {params.out_dir} {params.k_mer_length} -shortPaired -fastq.gz {input.fa1} -fastq.gz {input.fa2} {params.options} 2> {log}"

rule velvetg:
    input:
        rules.velveth.output
    output:
        contigs = "results/denovo_assembly/velvet/{sample}/contigs.fa"
    params:
        dir = "results/denovo_assembly/velvet/{sample}",
        options = config["denovo_assembly"]["Velvet"]["velvetg_options"]
    conda: "../envs/denovo_assembly.yaml"
    log: "results/logs/denovo_assembly/velvetg/{sample}.log"
    benchmark: "results/benchmarks/denovo_assembly/velvetg/{sample}.txt"
    shell:
        "velvetg {params} 2> {log}"

rule abyss:
    input:
        fa1 = choose_fa1_abyss,
        fa2 = choose_fa2_abyss
    output:
        contigs = "results/denovo_assembly/ABySS/{sample}/{sample}-contigs.fa"
    params:
        out_dir = "results/denovo_assembly/ABySS/{sample}",
        sample = "{sample}",
        k_mer_length = config["denovo_assembly"]["ABySS"]["k_mer"],
        bloom_filter_memory = config["denovo_assembly"]["ABySS"]["bloom_filter_memory"],
        options = config["denovo_assembly"]["ABySS"]["options"]
    conda: "../envs/denovo_assembly.yaml"
    log: "results/logs/denovo_assembly/ABySS/{sample}.log"
    benchmark: "results/benchmarks/denovo_assembly/abyss/{sample}.txt"
    threads: config["threads"]
    shell:
        "abyss-pe name={params.sample} -j {threads} k={params.k_mer_length} B={params.bloom_filter_memory} {params.options} -C {params.out_dir} in='{input.fa1} {input.fa2}' 2> {log}"

rule combine_contigs:
    input:
        spades = "results/denovo_assembly/SPAdes/{sample}/contigs.fasta",
        velvet = "results/denovo_assembly/velvet/{sample}/contigs.fa",
        abyss = "results/denovo_assembly/ABySS/{sample}/{sample}-contigs.fa"
    output: "results/denovo_assembly/{sample}/combined.fasta"
    conda: "../envs/denovo_assembly.yaml"
    log: "results/logs/denovo_assembly/combine_contigs/{sample}.log"
    benchmark: "results/benchmarks/denovo_assembly/combine_contigs/{sample}.txt"
    shell:
        "cat {input.spades} {input.velvet} {input.abyss} > {output} 2> {log}"

rule rename:
    input:
        rules.combine_contigs.output
    output: "results/denovo_assembly/{sample}/ren_combined.fasta"
    conda: "../envs/denovo_assembly.yaml"
    log: "results/logs/denovo_assembly/rename/{sample}.log"
    benchmark: "results/benchmarks/denovo_assembly/rename/{sample}.txt"
    shell:
        "seqkit rename {input} > {output} 2> {log}"

# long reads

rule spades_long:
    input: 
        long=choose_long,
        fa1 = choose_fa1,
        fa2 = choose_fa2
    output:
        contigs = "results/denovo_assembly/SPAdes_long/{sample}/contigs.fasta"
    params:
        out_dir = "results/denovo_assembly/SPAdes_long/{sample}",
        options = config["denovo_assembly"]["SPAdes"]["options"],
        tec = config["long_reads"]["technology"]
    conda: "../envs/denovo_assembly.yaml"
    log: "results/logs/denovo_assembly/SPAdes_long/{sample}.log"
    benchmark: "results/benchmarks/denovo_assembly/spades_long/{sample}.txt"
    threads: config["threads"]
    shell:
        "spades.py -1 {input.fa1} -2 {input.fa2} --{params.tec} {input.long} -t {threads} {params.options} -o {params.out_dir} 2> {log}"

# only long reads

rule flye:
    input: choose_long
    output: "results/denovo_assembly/flye/{sample}/assembly.fasta"
    params:
        tec=config["long_reads"]["technology"],
        out_dir="results/denovo_assembly/flye/{sample}",
        opt=config["denovo_assembly"]["Flye"]["options"]
    conda: "../envs/denovo_assembly.yaml"
    threads: config["threads"]
    shell:
        "flye --{params.tec}-raw {input} --out-dir {params.out_dir} --threads {threads} {params.opt} 2> {log}"