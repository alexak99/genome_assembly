rule eval_assemblies_denovo:
    input: 
        fb="results/variant_calling/{sample}/freebayes/consensus_freebayes.fa",
        ref_based_fb="results/ref_based_assembly/{sample}/consensus_freebayes.fa",
        cons="results/map_consens/{sample}/freebayes/consensus_freebayes.fa",
        spades="results/denovo_assembly/SPAdes/{sample}/contigs.fasta",
        velvet="results/denovo_assembly/velvet/{sample}/contigs.fa",
        abyss="results/denovo_assembly/ABySS/{sample}/{sample}-contigs.fa",
    output: directory("results/eval_assemblies/{sample}")
    params: config["QUAST"]["reference_dir"] + "/{sample}.fasta"
    conda: "../envs/eval_assemblies.yaml"
    log: "results/logs/QUAST/{sample}.log"
    threads: config["threads"]
    shell:
        "quast -o {output} -r {params} -t {threads} -l Pangenome_Graph,Comb-Assembly,Cons,SPAdes,Velvet,ABySS {input.fb} {input.ref_based_fb} {input.cons} {input.spades} {input.velvet} {input.abyss} 2> {log}"